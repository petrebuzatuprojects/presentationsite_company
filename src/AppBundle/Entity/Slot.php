<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Slot
 *
 * @ORM\Table(name="slot")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SlotRepository")
 */
class Slot
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name='';

    /**
     * @var SlotLanguage[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SlotLanguage" ,mappedBy="slot", cascade={"persist"}, orphanRemoval=true)
     */
    private $slotLanguages;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Slot
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return SlotLanguage[]
     */
    public function getSlotLanguages()
    {
        return $this->slotLanguages;
    }

    /**
     * @param SlotLanguage[] $slotLanguages
     * @return Slot
     */
    public function setSlotLanguages($slotLanguages)
    {
        $this->slotLanguages = $slotLanguages;
        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }


}

