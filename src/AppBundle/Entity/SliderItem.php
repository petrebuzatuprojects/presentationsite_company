<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * SliderItem
 *
 * @ORM\Table(name="slider_item")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SliderItemRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class SliderItem extends AbstractType
{

    const SERVER_PATH_TO_IMAGE_FOLDER = 'C:\xampp\htdocs\site-prezentare-ap\web\uploads';

    /**
     * Unmapped property to handle file uploads
     */
    private $file;

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }
    /**
     * SliderItem constructor.
     * @param SliderItem $sliderItems
     * @param SliderItemLanguage[] $sliderItemLanguages
     */
    public function __construct()
    {
        $this->sliderItems = new ArrayCollection();
        $this->sliderItemLanguages = new ArrayCollection();
    }

    /**
     * Manages the copying of the file to the relevant place on the server
     */
    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        // we use the original file name here but you should
        // sanitize it at least to avoid any security issues

        // move takes the target directory and target filename as params
        $this->getFile()->move(
            self::SERVER_PATH_TO_IMAGE_FOLDER,
            $this->getFile()->getClientOriginalName()
        );

        // set the path property to the filename where you've saved the file
        $this->image = $this->getFile()->getClientOriginalName();

        // clean up the file property as you won't need it anymore
        $this->setFile(null);
    }

    /**
     * @ORM\PrePersist
     */
    public function lifecycleFileUpload()
    {
        $this->upload();
    }

    /**
     * Updates the hash value to force the preUpdate and postUpdate events to fire.
     */
    public function refreshUpdated()
    {
        $this->setUpdated(new \DateTime());
    }

    /**
     * @var \DateTime|null
     */
    private $updated;



    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;

    /**
     * @var Slider
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Slider", inversedBy="slideritem")
     * @ORM\JoinColumn(name="slider_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $slider;

    /**
     * @var SliderItemLanguage[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SliderItemLanguage", mappedBy="sliderItem", cascade={"persist"},orphanRemoval=true)
     */
    private $sliderItemLanguages;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $image
     *
     * @return SliderItem
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return Slider
     */
    public function getSlider()
    {
        return $this->slider;
    }

    /**
     * @param Slider $slider
     * @return SliderItem
     */
    public function setSlider($slider)
    {
        $this->slider = $slider;
        return $this;
    }

    /**
     * @return SliderItemLanguage[]
     */
    public function getSliderItemLanguages()
    {
        return $this->sliderItemLanguages;
    }

    /**
     * @param SliderItemLanguage[] $sliderItemLanguages
     * @return SliderItem
     */
    public function setSliderItemLanguages($sliderItemLanguages)
    {
        $this->sliderItemLanguages = $sliderItemLanguages;
        return $this;
    }



    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime|null $updated
     * @return SliderItem
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
        return $this;
    }

    /**
     * @param int $id
     * @return SliderItem
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $url
     * @return SliderItem
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }



    public function addSliderItemLanguage(SliderItemLanguage $SliderItemLanguage){
        $this->sliderItemLanguages[] = $SliderItemLanguage;
        $SliderItemLanguage->setSliderItem($this);

        return $this;
    }

    public function removeSliderItemLanguage(SliderItemLanguage $SliderItemLanguage){
        $this->sliderItemLanguages->removeElement($SliderItemLanguage);
    }

}

