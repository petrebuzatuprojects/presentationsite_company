<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sliders
 *
 * @ORM\Table(name="slider")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SlidersRepository")
 */
class Slider
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name='';

    /**
     * @var SliderItem[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SliderItem",mappedBy="slider", cascade={"persist"},orphanRemoval=true)
     */
     private  $sliderItems;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Slider
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return SliderItem[]
     */
    public function getSliderItems()
    {
        return $this->sliderItems;
    }

    /**
     * @param SliderItem[] $sliderItems
     * @return Slider
     */
    public function setSliderItems($sliderItems)
    {
        $this->sliderItems = $sliderItems;
        return $this;
    }

    function __toString()
    {
        return $this->name;
    }

    /**
     * @param int $id
     * @return Slider
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


}

