<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SlotLanguage
 *
 * @ORM\Table(name="slot_language")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SlotLanguageRepository")
 */
class SlotLanguage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name='';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $language;

    /**
     * @var Slot
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Slot", inversedBy="slotLanguages")
     * @ORM\JoinColumn(name="slot_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $slot;

    function __get($description)
    {
        return $this->description;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SlotLanguage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SlotLanguage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return SlotLanguage
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return Slot
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * @param Slot $slot
     * @return SlotLanguage
     */
    public function setSlot($slot)
    {
        $this->slot = $slot;
        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }


}

