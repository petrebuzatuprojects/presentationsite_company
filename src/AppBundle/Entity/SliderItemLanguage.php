<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SliderItemLanguage
 *
 * @ORM\Table(name="slider_item_language")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SliderItemLanguageRepository")
 */
class SliderItemLanguage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Language",inversedBy="sliderItemLanguage")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $language;

    /**
     * @var SliderItem
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SliderItem", inversedBy="sliderItemLanguages")
     * @ORM\JoinColumn(name="slider_item_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $sliderItem;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name='';

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", length=255)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="titlu", type="string", length=255)
     */
    private $titlu;

    /**
     * @var string
     *
     * @ORM\Column(name="buttonName", type="string", length=255, nullable=true)
     */
    private $buttonName;

    /**
     * @return string
     */
    public function getButtonName()
    {
        return $this->buttonName;
    }

    /**
     * @param string $buttonName
     * @return SliderItemLanguage
     */
    public function setButtonName($buttonName)
    {
        $this->buttonName = $buttonName;
        return $this;
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return SliderItemLanguage
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param Language $language
     * @return SliderItemLanguage
     */
    public function setLanguage($language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return SliderItem
     */
    public function getSliderItem()
    {
        return $this->sliderItem;
    }

    /**
     * @param SliderItem $sliderItem
     * @return SliderItemLanguage
     */
    public function setSliderItem($sliderItem)
    {
        $this->sliderItem = $sliderItem;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return SliderItemLanguage
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return SliderItemLanguage
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitlu()
    {
        return $this->titlu;
    }

    /**
     * @param string $titlu
     * @return SliderItemLanguage
     */
    public function setTitlu($titlu)
    {
        $this->titlu = $titlu;
        return $this;
    }


    function __toString()
    {
        return $this->name;
    }
}

