<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Language
 *
 * @ORM\Table(name="language")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\LanguageRepository")
 */
class Language
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name='';


    /**
     * @var string
     *
     * @ORM\Column(name="flag", type="string", length=255)
     */
    private $flag='';


    /**
     * @var GaleryLanguage[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GaleryLanguage",mappedBy="language", cascade={"persist"},orphanRemoval=true)
     */
    private $galleries;


    /**
     * @var MenuItemLanguage[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MenuItemLanguage",mappedBy="language", cascade={"persist"},orphanRemoval=true)
     */
    private $menuItemLanguage;


    /**
     * @var SliderItemLanguage[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SliderItemLanguage",mappedBy="language", cascade={"persist"},orphanRemoval=true)
     */
    private  $sliderItemLanguage;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Language
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set flag
     *
     * @param string $flag
     *
     * @return Language
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return string
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * @return ContentLanguage
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param ContentLanguage $languages
     * @return Language
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;
        return $this;
    }

    /**
     * @param GaleryLanguage $galleres
     * @return Language
     */
    public function setGalleres($galleres)
    {
        $this->galleres = $galleres;
        return $this;
    }




    /**
     * @return SliderItemLanguage
     */
    public function getSliderItemLanguage()
    {
        return $this->sliderItemLanguage;
    }

    /**
     * @param SliderItemLanguage $sliderItemLanguage
     * @return Language
     */
    public function setSliderItemLanguage($sliderItemLanguage)
    {
        $this->sliderItemLanguage = $sliderItemLanguage;
        return $this;
    }

    /**
     * @return GaleryLanguage[]
     */
    public function getGalleries()
    {
        return $this->galleries;
    }

    /**
     * @param GaleryLanguage[] $galleries
     * @return Language
     */
    public function setGalleries($galleries)
    {
        $this->galleries = $galleries;
        return $this;
    }

    /**
     * @return MenuItemLanguage
     */
    public function getMenuItemLanguage()
    {
        return $this->menuItemLanguage;
    }

    /**
     * @param MenuItemLanguage $menuItemLanguage
     * @return Language
     */
    public function setMenuItemLanguage($menuItemLanguage)
    {
        $this->menuItemLanguage = $menuItemLanguage;
        return $this;
    }

    function __toString()
    {
        return $this->name;
    }


}

