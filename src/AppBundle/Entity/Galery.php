<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Galery
 *
 * @ORM\Table(name="galery")
 * @ORM\Entity()
 */
class Galery
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name='';

    /**
     * @var Content
     *
     * @ORM\OneToOne(targetEntity="Content", inversedBy="galery")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     */
    private $content;

    /**
     * @var GaleryItem[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GaleryItem", mappedBy="galery", cascade={"persist"},orphanRemoval=true)
     */
    private $galeryItems;

    function __toString()
    {

        return $this->name;
    }


    /**
     * @var GaleryLanguage[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GaleryLanguage", mappedBy="galery", cascade={"persist"},orphanRemoval=true)
     */
    private $galeryLanguages;

    /**
     * Galery constructor.
     * @param GaleryItem $galeryItems
     * @param GaleryLanguage[] $galeryLanguages
     */
    public function __construct()
    {
        $this->galeryItems = new ArrayCollection();
        $this->galeryLanguages = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Galery
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return GaleryItem[]
     */
    public function getGaleryItems()
    {
        return $this->galeryItems;
    }

    /**
     * @param GaleryItem[] $galeryItems
     * @return Galery
     */
    public function setGaleryItems($galeryItems)
    {
        $this->galeryItems = $galeryItems;
        return $this;
    }

    public function addGaleryItem(GaleryItem $galeryItem){
        $this->galeryItems[] = $galeryItem;
        $galeryItem->setGalery($this);

        return $this;
    }

    public function removeGaleryItem(GaleryItem $galeryItem){
        $this->galeryItems->removeElement($galeryItem);
    }

    /**
     * @return GaleryLanguage[]
     */
    public function getGaleryLanguages()
    {
        return $this->galeryLanguages;
    }

    /**
     * @param GaleryLanguage[] $galeryLanguages
     * @return Galery
     */
    public function setGaleryLanguages($galeryLanguages)
    {
        $this->galeryLanguages = $galeryLanguages;
        return $this;
    }

    public function addGaleryLanguage(GaleryLanguage $galeryLanguage){
        $this->galeryLanguages[] = $galeryLanguage;
        $galeryLanguage->setGalery($this);

        return $this;
    }

    public function removeGaleryLanguage(GaleryLanguage $galeryLanguage){
        $this->galeryLanguages->removeElement($galeryLanguage);
    }



    public function getFirstImage(){
        if ($this->galeryItems->first()) {
            return $this->galeryItems->first();
        } else {
            $noImage = new GaleryItem();
            $noImage->setImage('no_image.png');

            return $noImage;
        }
    }

    /**
     * @return Content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param Content $content
     * @return Galery
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }





}

