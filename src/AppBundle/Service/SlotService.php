<?php


namespace AppBundle\Service;


use AppBundle\Entity\Slot;
use AppBundle\Entity\SlotLanguage;
use Doctrine\Common\Persistence\ManagerRegistry;

class SlotService
{

    /** @var  ManagerRegistry */
    private $entityManager;


    private $session;


    public function getSlot()
    {
        return $this->getEntityManager()->getRepository(SlotLanguage::class)->findAll();
    }

    public function getSlotItem($name){
        $list=[];
        $slots = $this->getEntityManager()->getRepository(Slot::class)->findOneBy(['name'=>$name]);

        foreach($slots->getSlotLanguages() as $slotLanguage)
        {
                  $list[]=$slotLanguage;
        }

        return $list;
    }



    /**
     * @return ManagerRegistry
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param ManagerRegistry $entityManager
     * @return SlotService
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * @param mixed $session
     * @return SlotService
     */
    public function setSession($session)
    {
        $this->session = $session;
        return $this;
    }




}