<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ContentLanguageAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('description')
            ->add('keywords')
            ->add('title')
           // ->add('htmlContent')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

            ->add('description')
            ->add('keywords')
            ->add('title')
            //->add('htmlContent')

            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],

                ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper



            ->add('content')
            ->add('language')
            ->add('title')
            ->add('keywords')
            ->add('description')
            ->add('htmlContent', 'textarea',
                array('required'=>false,'attr' => array('class' => 'tinymce-enabled')));


    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('description')
            ->add('keywords')
            ->add('title')
            //->add('htmlContent')

        ;
    }
}
