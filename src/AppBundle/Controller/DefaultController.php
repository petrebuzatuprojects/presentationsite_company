<?php

namespace AppBundle\Controller;
use AppBundle\Entity\Content;
use AppBundle\Entity\Galery;
use AppBundle\Entity\GaleryLanguage;
use AppBundle\Entity\GaleryItem;
use AppBundle\Entity\Slider;
use FOS\UserBundle\Controller\SecurityController;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use AppBundle\Entity\Contact;

class DefaultController extends SecurityController
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $sliders = $em->getRepository(Slider::class)->findOneBy(['name' => 'Main Slider']);

        $sliderItems = $sliders->getSliderItems();

        return $this->render('default/index.html.twig', ['sliderItems' => $sliderItems]);

    }


    /**
     * @Route("language/{name}", name="language")
     */
    public function languageAction(Request $request, $name = 'Ro')
    {

        $this->get('session')->set('_locale', $name);

        $request->setLocale($name);

        return $this->redirect($request->headers->get('referer'));

    }


    /**
     * @Route("menu/{url}", name="menu")
     */

    public function switchMenuAction(Request $request, $url = '', $language = 'en')
    {

        $this->get('session')->set('_locale', $url);
        $request->setLocale($url);
        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * @Route("content/{name}", name="content")
     */
    public function contentAction(Request $request, $name)
    {

        $em = $this->getDoctrine()->getManager();
        $content = $em->getRepository(Content::class)->findOneBy(['name' => $name]);
        foreach ($content->getContentLanguages() as $contentLanguage) {
            if ($this->get('session')->get('_locale', 'Ro') == $contentLanguage->getLanguage()->getName()) {
                $cl = $contentLanguage;
            }


        if (!isset($cl)) {
            $cl = $content->getContentLanguages()[0];
        }

            return $this->render('default/content.html.twig', ['contentLanguage' => $cl]);
        }
    }


    /**
     * @Route ("galery/{id}", name="galerySingle")
     */
    public function galerySingleAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $galery = $em->getRepository(galery::class)->find($id);

        $galeryItems = $em->getRepository(galeryItem::class)->findAll();

        $galeryLanguages = $galery->getContent()->getContentLanguages();

        foreach ($galeryLanguages as $galeryLanguage) {

            if ($this->get('session')->get('_locale', 'Ro') == $galeryLanguage->getLanguage()->getName()) {
                $gl = $galeryLanguage;
            }
        }


        if (!isset($gl)){
            $gl = $galeryLanguage->getLanguage()[0];

        }

        return $this->render('default/galerySingle.html.twig', ['galery' => $galery, 'galeryItems' => $galeryItems, 'gl' => $gl]);
    }


    /**
     * @Route("galery", name="galery")
     */
    public function galeryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $galeries = $em->getRepository(galery::class)->findAll();
        $galeryLanguages = $em->getRepository(galeryLanguage::class)->findAll();
        foreach( $galeryLanguages as $galeryLanguage ){

            if ($this->get('session')->get('_locale', 'Ro') == $galeryLanguage->getLanguage()->getName()) {
                $gl=$galeryLanguage;
            }

        }

        if (!isset($gl)){
            $gl=$galeryLanguage->getLanguage()[0];
        }



        return $this->render('default/galery.html.twig', ['galeries' => $galeries,'gl'=>$gl]);

    }


    /**
     * @Route("/login", name="fos_user_security_login")
     */
    public function UserLoggedAction(Request $request)
    {
        $tokenInterface = $this->get('security.token_storage')->getToken();

        if (!$tokenInterface instanceof AnonymousToken) {
            return $this->redirectToRoute('sonata_admin_dashboard');
        } else {
            return $this->loginAction($request);
        }

    }



    /**
     * @Route("/contact", name="contactPage")
     */
    public function createAction(Request $request)
    {
        $contact = new Contact();
        # Add form fields
        $form = $this->createFormBuilder($contact)
            ->add('name', TextType::class, array('label' => 'name', 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('email', EmailType::class, array('label' => 'email', 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('subject', TextType::class, array('label' => 'subject', 'attr' => array('class' => 'form-control', 'style' => 'margin-bottom:15px')))
            ->add('message', TextareaType::class, array('label' => 'message', 'attr' => array('class' => 'form-control')))
            ->add('Save', SubmitType::class, array('label' => 'submit', 'attr' => array('class' => 'btn btn-primary', 'style' => 'margin-top:15px')))
            ->getForm();
        $form->handleRequest($request);

        if($form->isSubmitted() &&  $form->isValid()){
            $contact = $form->getData();
            $name = $form['name']->getData();
            $email = $form['email']->getData();
            $subject = $form['subject']->getData();
            $message = $form['message']->getData();
//
//            # set form data
//            $contact->setName($name);
//            $contact->setEmail($email);
//            $contact->setSubject($subject);
//            $contact->setMessage($message);
//
//            # finally add data in database
//            $sn = $this->getDoctrine()->getManager();
//            $sn -> persist($contact);
//            $sn -> flush();
//
            $message = \Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($email)
                ->setTo('daniel@ianosi.ro')
                ->setBody($this->renderView('default/mail.html.twig',array('name' => $name, 'email' => $email, 'message'=>$message)),'text/html');
            $this->get('mailer')->send($message);

            //return $this->redirectToRoute('index');
            $name = $form->getData();
            return $this->render('default/sendEmail.html.twig', ['name' => $name]);
        }
        # Handle form response
        return $this->render('default/contact.html.twig', ['form' => $form->createView(),]);
    }
}







